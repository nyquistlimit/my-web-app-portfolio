
var app = angular.module('portfolioWebApp', [
  'ngRoute'
]);
/**
 * Configure the Routes
 */
app.config(['$routeProvider', function ($routeProvider) {
  $routeProvider
    // Home
    .when("/", {templateUrl: "partials/home.min.html", controller: "PageCtrl"})
    // Pages
    .when("/about", {templateUrl: "partials/about.min.html", controller: "PageCtrl"})
    .when("/web", {templateUrl: "partials/web.min.html", controller: "PageCtrl"})
    .when("/android", {templateUrl: "partials/android.min.html", controller: "PageCtrl"})
    .when("/contact", {templateUrl: "partials/contact.min.html", controller: "PageCtrl"})
    // else 404
    .otherwise("/404", {templateUrl: "partials/404.min.html", controller: "PageCtrl"});
}]);

/**
 * Controls all other Pages
 */
app.controller('PageCtrl', function (/* $scope, $location, $http */) {
  console.log("Page Controller reporting for duty.");

  // Activates the Carousel
  $('.carousel').carousel({
    interval: 5000
  });

  // Activates Tooltips for Social Links
  $('.tooltip-social').tooltip({
    selector: "a[data-toggle=tooltip]"
  })
});

